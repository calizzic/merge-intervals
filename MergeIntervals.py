class Solution1(object):
    def merge(self, intervals):
        """
        Optimized & elegant O(Nlog(N)) solution using sorting
        """
        result = []
        intervals.sort()
        if not intervals:
            return []
        i = 1
        result.append(intervals[0])
        while i<len(intervals):
            top = result.pop()
            if(top[1]>=intervals[i][1]):
                result.append(top)
            elif(top[1]>=intervals[i][0]):
                top[1] = intervals[i][1]
                result.append(top)
            else:
                result.append(top)
                result.append(intervals[i])
            i+=1
        return result

class Solution2(object):
    def merge(self, intervals):
        """
        Original Solution without sorting
        -Only need to loop through original once
        -Build as you go? Add one interval at a time, can do binary search 
            -Compare once start is after the start of the previous interval
            -new interval, connect existing intervals, or merge interval and all intervals after
            such that end>start
            -Check previous interval first, 
            
        example:
        1,2 3,4 4,7 2,9
        1,2
        1,2 3,4
        
        """
        result = []
        #Loop through original
        for interval in intervals:
            i = 0
            #Find index of first element with a greater start (len(result) if none)
            while i<len(result) and result[i][0]<interval[0]:
                i+=1
            #check i-1
            greater = True
            if i!=0 and result[i-1][1]>=interval[0]:
                greater = interval[1]>result[i-1][1]
                if greater:
                    i = i-1
                    result[i][1] = interval[1]
            else:
                result.insert(i,interval)
            #interval is merged in, result is correct through index i
            #absorb elements to the right until correct or last element
            while greater and i<len(result)-1:
                if result[i+1][1]<result[i][1]:
                    result.pop(i+1)
                else:
                    greater = False
                    if result[i+1][0]<=result[i][1]:
                        result[i][1] = result[i+1][1]
                        result.pop(i+1)
                    #else do nothing, overlaps are resolved
        return result